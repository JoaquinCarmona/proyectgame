<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('metodologias', 'metodologiaController');

Route::resource('temas', 'temaController');

Route::resource('retos', 'retoController');

Route::resource('preguntaretos', 'preguntaretoController');

Route::get('jugador/home', 'JugadorController@home')->name('jhome');
Route::get('juego/inicio', 'JugadorController@inicio_juego')->name('inicio_juego');
Route::get('juego/{metodologia}', 'JugadorController@inicio_metodologia')->name('inicio_metodologia');
