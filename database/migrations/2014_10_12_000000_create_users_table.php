<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('type')->nullable(); // 1 Administrador, 2 Jugador
            $table->string('imagen')->nullable();
            $table->string('avatar')->nullable(); // imagen del avatar
            $table->string('nivel')->nullable();; //Nivel en el juego
            $table->date('fecha_nacimiento');
            $table->text('razon_proyectos');
            $table->text('nivel_estudios');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
