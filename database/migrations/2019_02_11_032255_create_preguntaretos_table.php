<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatepreguntaretosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntaretos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reto_id');
            $table->string('pregunta');
            $table->string('respuesta_1');
            $table->string('respuesta_2');
            $table->integer('respuesta_correcta');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('preguntaretos');
    }
}
