<table class="table table-responsive" id="metodologias-table">
    <thead>
        <tr>
            <th>Nombre</th>
        <th>Descripcion</th>
        <th>Imagen</th>
        <th>Nivel</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($metodologias as $metodologia)
        <tr>
            <td>{!! $metodologia->nombre !!}</td>
            <td>{!! $metodologia->descripcion !!}</td>
            <td>{!! $metodologia->imagen !!}</td>
            <td>{!! $metodologia->nivel !!}</td>
            <td>
                {!! Form::open(['route' => ['metodologias.destroy', $metodologia->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('metodologias.show', [$metodologia->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('metodologias.edit', [$metodologia->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>