<!DOCTYPE HTML>
<html lang="en">

<head>
    <script> 
function abrir() { 
open('pagina.html','','top=300,left=300,width=300,height=300') ; 
} 
</script>
    <meta charset="utf-8">
    <title>Innovatium - project Game</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="color/default.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/logo.ico">
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="font-awesome.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="main.js"></script>
        <link rel="stylesheet" href="csss/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:700,800,400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/style1.css">
    <link rel="stylesheet" href="css/seleccionar-imagen.css">

    @yield('css')
    <link href="css/bootstrap.min2.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Custom styles for this template -->
        
        <link href="css/style4.css" rel="stylesheet">
    
    
    <script type="text/javascript" src="js/modernizr.custom.79639.js"></script>
    <noscript>
    <link rel="stylesheet" type="text/css" href="css/styleNoJS.css" />
    </noscript>
     
</head>

<body>

    <!-- navbar -->
    <!--  <div class="navbar-wrapper">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <!-- Responsive navbar -->
<!--                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </a>
                    <h1 class="brand"><img src="img/blanco.png" width="200" height="50" alt="" /></h1>
                    <!-- navigation -->
    <!--                <nav class="pull-right nav-collapse collapse">
                        <ul id="menu-main" class="nav">
                            <li><a title="team" href="#header-wrapper">Inicio</a></li>
                            <li><a title="services" href="#works">Servicios</a></li>
                            <li><a title="works" href="#about">nosotros</a></li>
                            <li><a title="blog" href="#services">clientes</a></li>
                            <li><a title="contact" href="#contact">Contacto</a></li>
                        
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>-->
    
    
    <!-- navbar -->
            <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container" style="margin-top: 10px" >
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top"><img src="img/blanco.png" style="height: 40px; width: 200px;" alt="innovatium"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>


                        @if (Route::has('login'))
                    @auth
                        <li>
                            <a class="page-scroll" href="{{ url('/home') }}">Jugar</a>
                        </li>
                    @else
                        <li>
                            <a class="page-scroll" href="{{ route('login') }}">Iniciar sesión</a>
                        </li>

                        @if (Route::has('register'))
                            <li>
                                <a class="page-scroll" href="{{ route('register') }}">Registrarme</a>
                            </li>
                        @endif
                    @endauth
            @endif
                    </ul>
                </div>
            </div>
        </nav>



        @yield('content')


 <footer>
       
       <div class="container-footer-all">
        
            <div class="container-body">

                <div class="colum1">
                    <h1>Información de Contacto</h1>

                    
                     <div class="row2">
                        <img src="img/icons/smartphone.png">
                        <label>+ 52 (55) 85261012
</label>
                    </div>
                    <div class="row2">
                        <img src="img/icons/smartphone.png">
                        <label>+ 52 (777) 1763415</label>
                    </div>
                    
                      <div class="row2">
                        <img src="img/icons/contact.png">
                         <label>contacto@innovatium.mx</label>
                    </div> 


                </div>
                

                <div class="colum2">

                    <h1 style="text-decoration: none">Redes Sociales</h1>

                    <div class="row1">
                        <a href="https://www.facebook.com/innovatiumcertificaciones/" target="_blank"><img src="img/icons/facebook.png"></a>
                        <a href="https://www.facebook.com/innovatiumcertificaciones/" target="_blank"><label>Siguenos en Facebook</label></a>
                    </div>
                    <div class="row1">
                        <a href="https://www.youtube.com/channel/UCDVe-gjR8CfCB5TVlxdHnuA?view_as=subscriber" target="_blank"> <img src="img/icons/yo.png"></a>
                           <a href="https://www.youtube.com/channel/UCDVe-gjR8CfCB5TVlxdHnuA?view_as=subscriber" target="_blank"><label>Siguenos en Youtube</label></a>
                    </div>
                     <div class="row2">
                         <img src="img/icons/wh.png" >
                           <label>+52 1 55 6941 7110</label>
                    </div>
                    
<a href="https://www.facebook.com/innovatiumcertificaciones/" target="_blank"></a>

                </div>

                <div class="colum3">

                    <h1>&nbsp;&nbsp;&nbsp;&nbsp; Oficina Corporativa</h1>

                    <div class="row2">
                        <img src="img/icons/house.png">
                        <label>Sonora No. 300, Col. Vista Hermosa,
Cuernavaca, Morelos, México.</label>
                    </div>


                </div>

            </div>
        
        </div>
        
        <div class="container-footer">
               <div class="footer">
                    <div class="copyright" style="align-content: center;">
                        © Todos los Derechos Reservados | INNOVATIUM.

                   
                </div>

            </div>
        
    </footer>
        
    
    <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.scrollTo.js"></script>
    <script src="js/jquery.nav.js"></script>
    <script src="js/jquery.localScroll.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/isotope.js"></script>
    <script src="js/jquery.flexslider.js"></script>
    <script src="js/inview.js"></script>
    <script src="js/animate.js"></script>
    <script src="js/custom.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/velocity/1.1.0/velocity.min.js'></script>
    <script  src="js/index.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.ba-cond.min.js"></script>
        <script type="text/javascript" src="js/jquery.slitslider.js"></script>
       

    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="js/bootstrap.min2.js"></script>
        <script src="js/seleccionar-imagen.js"></script>
</body>

</html>

