@extends('layouts.app')

@section('content')


    <div class="row jumbotron">

        <div class="panel col-md-10 col-md-offset-1" align="center">



            <div class="col-md-6 panel-default">

                {!! Html::image('img/avatar/'.Auth::user()->avatar, 'Avatar', array('width' => '250')) !!}
                <br/>
                <h1><span class="label label-success"> {{Auth::user()->alias."    Lv. ".Auth::user()->nivel}}</span></h1>
                <br/>

            </div>
            <div class="col-md-6 panel-default">

                <div class="col-md-12 panel label-default" style="margin: 10px;">

                    <h2>Bienvenido <strong>{{Auth::user()->name}}</strong></h2>
                    <p>Este es un juego para aprender a gestionar proyectos, con metodologías agiles</p>

                </div>

            </div>

        </div>



    </div>

@endsection


