@extends('layouts.app')

@section('content')

    <div class="row jumbotron">

        <div class="col-md-10 col-md-offset-1" align="center">

            @foreach($metodologias as $metodologia)

                <div class="panel col-md-3" align="center">

                    <a href="{!! route('inicio_metodologia',['$metodologia' => $metodologia->id]) !!}">

                        <div class="panel-body">
                            <div>
                                {!! Html::image('img/mundo/m1.png', 'Avatar', array('class'=>'col-md-12')) !!}
                            </div>
                            <br>
                            <div>
                                <h3>{!! $metodologia->nombre !!}</h3>
                                <strong>Nivel: </strong> {!! $metodologia->nivel !!}
                            </div>
                        </div>

                    </a>

                </div>
            @endforeach

        </div>

    </div>

@endsection


