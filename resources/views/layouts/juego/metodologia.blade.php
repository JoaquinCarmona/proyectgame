@extends('layouts.app')

@section('content')

    <div class="row jumbotron">

        <div class="panel col-md-10 col-md-offset-1" align="center">

            <h1>{{ $metodologia->nombre }}</h1>
            <div class="col-md-12" align="left">

                <p>
                    <strong>Descripción: </strong>{{$metodologia->descripcion}}
                </p>

                <div style="margin: 10px;" align="right">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#TemasJuego">Guia</button>
                </div>

                <br>

                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

                            <h3>Retos</h3>

                        </div>

                        <div class="collapse panel-body" id="collapseExample">

                            @foreach( $retos as $reto )

                                <div class="well-small">

                                    <div class="col-md-12">

                                        <div class="col-md-6">

                                            <h4>{{$reto->nombre}}</h4>

                                        </div>
                                        <div class="col-md-4">

                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                                    0 de {!!count ($preguntas->where('reto_id', $reto->id))!!}
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-2">

                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#{{'reto'.$reto->id}}">Iniciar</button>

                                        </div>

                                    </div>

                                </div>

                            @endforeach

                        </div>

                    </div>




                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="TemasJuego" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div id="carousel-info-temas" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="true" data-keyboard="true">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <div class="well"   style="height:200px;max-height: 200px;overflow-y: scroll;">
                                            <h4>Master: </h4>
                                            <p>
                                                Hola {!! Auth::user()->name !!}, yo soy Master seré tu maestro. Espero puedas aprender mucho.
                                            </p>
                                        </div>
                                    </div>

                                    @foreach( $temas as $tema )

                                        <div class="item">
                                            <div class="well" style="height:200px;max-height: 200px;overflow-y: scroll;">
                                                <h4>Master: </h4>
                                                <p>
                                                    Veremos el tema {{$tema->titulo}}
                                                </p>
                                            </div>
                                        </div>

                                        <div class="item">
                                            <div class="well" style="height:200px;max-height: 200px;overflow-y: scroll;">
                                                <h4>Master: </h4>
                                                <p>
                                                    {{$tema->descripcion}}
                                                </p>
                                            </div>
                                        </div>

                                    @endforeach

                                </div>
                            </div>

                            <div class="col-md-12" align="center">
                                <button id="temas-anterior" class="btn btn-default" href="#carousel-info-temas" role="button" data-slide="prev"><</button>
                                <button id="temas-siguiente" class="btn btn-default" href="#carousel-info-temas" role="button" data-slide="next">></button>
                            </div>



                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-6">
                        {!! Html::image('img/avatar/'.Auth::user()->avatar, 'Avatar', array('class' => 'col-md-12')) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Html::image('img/avatar/maestro.png', 'Avatar', array('class' => 'col-md-12')) !!}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>

        checkitem();

        $("#TemasJuego").on("slid.bs.carousel", "", checkitem);
        $('#TemasJuego').on('slid', '', checkitem);

        function checkitem()                        // check function
        {
            console.log("hola");

            var $this = $('#TemasJuego');
            if ($('.carousel-inner .item:first').hasClass('active')) {
                $('#temas-anterior').hide();
                console.log("hola1");
            } else if ($('.carousel-inner .item:last').hasClass('active')) {
                $('#temas-siguiente').hide();
                console.log("hola2");
            } else {
                $('#temas-anterior').show();
                $('#temas-siguiente').show();
                console.log("hola3");

            }
        }
    </script>

    @foreach( $retos as $reto )


        <div class="modal fade" id="{{'reto'.$reto->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-body">
                                <div id="carousel-{{'reto'.$reto->id}}" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="true" data-keyboard="true">
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <div class="item active">
                                            <div class="well"   style="height:200px;max-height: 200px;overflow-y: scroll;">
                                                <h4>Malo malote: </h4>
                                                <p>
                                                    Hey {!! Auth::user()->name !!}, por fin yo Malo malote te derrotare.
                                                </p>
                                            </div>
                                        </div>

                                        @foreach( $preguntas->where('reto_id', $reto->id) as $pregunta )

                                            <div class="item">
                                                <div class="well" style="height:200px;max-height: 200px;overflow-y: scroll;">
                                                    <h4>Malo malote: </h4>
                                                    <p>
                                                        <strong>{{$pregunta->pregunta}}</strong>
                                                    </p>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <input type="radio" value="{{$pregunta->respuesta_1}}" name="{{'r'.$pregunta->id}}"> {{$pregunta->respuesta_1}}
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="radio" value="{{$pregunta->respuesta_2}}" name="{{'r'.$pregunta->id}}"> 
                                                            {{$pregunta->respuesta_2}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>

                                <div class="col-md-12" align="center">
                                    <button id="temas-anterior-{{'reto'.$reto->id}}" class="btn btn-default" href="#carousel-{{'reto'.$reto->id}}" role="button" data-slide="prev"><</button>
                                    <button id="temas-siguiente-{{'reto'.$reto->id}}" class="btn btn-default" href="#carousel-{{'reto'.$reto->id}}" role="button" data-slide="next">></button>
                                </div>



                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            {!! Html::image('img/avatar/'.Auth::user()->avatar, 'Avatar', array('class' => 'col-md-12')) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Html::image('img/avatar/malo.png', 'Avatar', array('class' => 'col-md-12')) !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endforeach

@endsection


