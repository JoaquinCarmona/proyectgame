<li class="{{ Request::is('metodologias*') ? 'active' : '' }}">
    <a href="{!! route('metodologias.index') !!}"><i class="fa fa-edit"></i><span>Metodologias</span></a>
</li>

<li class="{{ Request::is('temas*') ? 'active' : '' }}">
    <a href="{!! route('temas.index') !!}"><i class="fa fa-edit"></i><span>Temas</span></a>
</li>

<li class="{{ Request::is('retos*') ? 'active' : '' }}">
    <a href="{!! route('retos.index') !!}"><i class="fa fa-edit"></i><span>Retos</span></a>
</li>

<li class="{{ Request::is('preguntaretos*') ? 'active' : '' }}">
    <a href="{!! route('preguntaretos.index') !!}"><i class="fa fa-edit"></i><span>Preguntaretos</span></a>
</li>

