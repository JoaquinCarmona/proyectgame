@extends('layouts.appl')

@section('content')

<section id="contact" class="section green">
        <div class="container">
            <h4  >Inicio Sesión</h4>
            <p align="center" >
                <!--Mandanos un mensaje y nosotros nos comunicaremos contigo.-->
            </p>
            <div class="blankdivider30">
            </div>
            <div class="row">
                <div class="span12">
                    <div class="cform" id="contact-form">
                            <form method="post" action="{{ url('/login') }}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="span4 offset4 text-center">
                                    <div class="field your-name form-group">
                                        <input type="email" class="form-control inputtext" name="email" value="{{ old('email') }}" placeholder="Email" data-rule="email" data-msg="Ingrese un email valido" required/>
                                        
                                        @if ($errors->has('email'))
                                            <div class="validation"></div>
                                            <span style="color: white;">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    <div class="field your-email form-group">
                                        <input type="password" class="form-control inputtext" placeholder="Password" name="password" data-rule="minlen:6" data-msg="Por favor ingresa minimo 6 caracteres" required/>
                                        @if ($errors->has('password'))
                                            <div class="validation"></div>
                                            <span style="color: white;">{{ $errors->first('password') }}</span>
                                        @endif
                                        
                                    </div>
                                    <div class="text-center" >
                                    <input type="submit" value="Iniciar sesion" class="btn btn-theme text-center" style="margin: 0 auto; background-color: black !important;"></div>
                                    <!--<a href="{{ url('/password/reset') }}">Olvidé mi contraseña</a><br>-->
        <a href="{{ url('/register') }}" class="text-center">Registrarme</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div></div>
    </section>


@endsection


@section('css')
<!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">


    <style type="text/css">
        .inputtext {
            width: 100% !important;
    padding: 0.5em !important;
    font-size: 1em !important;
    margin-bottom: 0.5em !important;
    box-sizing: border-box !important;
    -moz-box-sizing: border-box !important;
    -webkit-box-sizing: border-box !important;

    background: rgba(227, 231, 228, 1) !important;
    font-family: 'Open Sans', sans serif !important;
    border: 0 !important;
    font-size:16px !important;
    text-align: left !important;
    vertical-align: middle !important;
    height: 2.2em !important;
        }
    </style>
@endsection