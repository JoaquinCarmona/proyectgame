

@extends('layouts.appl')

@section('content')
<section id="contact" class="section green">
        <div class="container">
            <h4  >Registrar nuevo usuario</h4>
            <p align="center" >
                <!--Mandanos un mensaje y nosotros nos comunicaremos contigo.-->
            </p>
            <div class="blankdivider30">
            </div>
            <div class="row">
                <div class="span6 offset3">
                    <div class="cform" id="contact-form">
                            <form method="post" action="{{ url('/register') }}" files="true" enctype="multipart/form-data">

                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="form-group" style="margin-bottom: 0px">Nombre completo</label>
                                <input type="text" class="form-control inputtext" name="name" value="{{ old('name') }}" placeholder="Nombre completo" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <span style="color: white;">{{ $errors->first('name') }}</span> 
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-group" style="margin-bottom: 0px">Email</label>
                                <input type="email" class="form-control inputtext" name="email" value="{{ old('email') }}" placeholder="Email" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <span style="color: white;">{{ $errors->first('email') }}</span> 
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-group" style="margin-bottom: 0px">Password</label>
                                <input type="password" class="form-control inputtext" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <span style="color: white;">{{ $errors->first('password') }}</span> 
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-group" style="margin-bottom: 0px">Confirma tu password</label>
                                <input type="password" name="password_confirmation" class="form-control inputtext" placeholder="Confirmar password" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <span style="color: white;">{{ $errors->first('password_confirmation') }}</span> 
                                    </span>
                                @endif
                            </div>



                            <div class="form-group">
                                <label class="form-group" style="margin-bottom: 0px">Alias (Así te verán los demás jugadores)</label>
                                <input type="text" class="form-control inputtext" name="alias" placeholder="Alias" required>
                                <input type="text" class="form-control inputtext" name="type" placeholder="Alias" value="2" style="display: none;">
                                <input type="text" class="form-control inputtext" name="nivel" placeholder="Alias" value="1" style="display: none;">
                                <input type="text" id="data-imagen-seleccionada" class="form-control inputtext" name="avatar" placeholder="Avatar" value="h1.png" required style="display: none;">
                                <div class="form-group">

                                    <div class="row">

                                            <img id="h1" class="imagen-opcion col-sm-3 imagen-seleccionada" src="img/avatar/h1.png">
                                            <img id="h2" class="imagen-opcion col-sm-3" src="img/avatar/h2.png">
                                            <img id="h3" class="imagen-opcion col-sm-3" src="img/avatar/h3.png">
                                            <img id="m1" class="imagen-opcion col-sm-3" src="img/avatar/m1.png">
                                            <img id="m2" class="imagen-opcion col-sm-3" src="img/avatar/m2.png">
                                            <img id="m3" class="imagen-opcion col-sm-3" src="img/avatar/m3.png">

                                    </div>

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="form-group" style="margin-bottom: 0px">Fecha de nacimiento</label>
                                <input type="date" class="form-control inputtext" name="fecha_nacimiento" placeholder="Fecha de nacimiento" required>
                            </div>
                            <div class="form-group">
                                <label class="form-group" style="margin-bottom: 0px">Nivel de estudios</label>
                                <select class="form-group inputtext" name="nivel_estudios" required>
                                    <option value="0" selected disabled>Nivel de estudios</option>
                                    <option value="1">Primaria</option>
                                    <option value="2">Secundaria</option>
                                    <option value="3">Preparatoria</option>
                                    <option value="4">Licenciatura</option>
                                    <option value="5">Posgrado</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-group" style="margin-bottom: 0px">Escriba brevemente la razón por la que desea aprender administración de proyectos</label>
                                <textarea class="form-group" style="background: rgba(227, 231, 228, 1) !important;" name="razon_proyectos" required></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-group" style="margin-bottom: 0px">Sube tu AVATAR (La imágen con la que te identificarás en la comunidad) ** No podrás cambiar tu avatar hasta el nivel 10</label>
                                <input type="file" name="imagen" class="form-control inputtext" style="height: 3em !important;" required="">
                            </div>
                            <div class="text-center">
                                    <input type="submit" value="Registrar" class="btn btn-theme text-center" style="margin: 0 auto;background-color: black !important;"></div>
                                <!-- /.col -->
                            </div>
                        </form>
                    </div>
                </div>
            </div></div>
    </section>


@endsection

@section('css')
<!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">


    <style type="text/css">
        .inputtext {
            width: 100% !important;
    padding: 0.5em !important;
    font-size: 1em !important;
    margin-bottom: 0.5em !important;
    box-sizing: border-box !important;
    -moz-box-sizing: border-box !important;
    -webkit-box-sizing: border-box !important;

    background: rgba(227, 231, 228, 1) !important;
    font-family: 'Open Sans', sans serif !important;
    border: 0 !important;
    font-size:16px !important;
    text-align: left !important;
    vertical-align: middle !important;
    height: 2.2em !important;
        }
    </style>


@endsection
