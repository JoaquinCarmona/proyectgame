<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Tema Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tema_id', 'Tema Id:') !!}
    {!! Form::number('tema_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Requisitos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('requisitos', 'Requisitos:') !!}
    {!! Form::text('requisitos', null, ['class' => 'form-control']) !!}
</div>

<!-- Requisito Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('requisito_number', 'Requisito Number:') !!}
    {!! Form::number('requisito_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('retos.index') !!}" class="btn btn-default">Cancel</a>
</div>
