<table class="table table-responsive" id="retos-table">
    <thead>
        <tr>
            <th>Nombre</th>
        <th>Tema Id</th>
        <th>Descripcion</th>
        <th>Requisitos</th>
        <th>Requisito Number</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($retos as $reto)
        <tr>
            <td>{!! $reto->nombre !!}</td>
            <td>{!! $reto->tema_id !!}</td>
            <td>{!! $reto->descripcion !!}</td>
            <td>{!! $reto->requisitos !!}</td>
            <td>{!! $reto->requisito_number !!}</td>
            <td>
                {!! Form::open(['route' => ['retos.destroy', $reto->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('retos.show', [$reto->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('retos.edit', [$reto->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>