<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $reto->id !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $reto->nombre !!}</p>
</div>

<!-- Tema Id Field -->
<div class="form-group">
    {!! Form::label('tema_id', 'Tema Id:') !!}
    <p>{!! $reto->tema_id !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $reto->descripcion !!}</p>
</div>

<!-- Requisitos Field -->
<div class="form-group">
    {!! Form::label('requisitos', 'Requisitos:') !!}
    <p>{!! $reto->requisitos !!}</p>
</div>

<!-- Requisito Number Field -->
<div class="form-group">
    {!! Form::label('requisito_number', 'Requisito Number:') !!}
    <p>{!! $reto->requisito_number !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $reto->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $reto->updated_at !!}</p>
</div>

