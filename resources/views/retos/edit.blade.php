@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reto
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($reto, ['route' => ['retos.update', $reto->id], 'method' => 'patch']) !!}

                        @include('retos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection