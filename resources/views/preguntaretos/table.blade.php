<table class="table table-responsive" id="preguntaretos-table">
    <thead>
        <tr>
            <th>Reto Id</th>
        <th>Pregunta</th>
        <th>Respuesta 1</th>
        <th>Respuesta 2</th>
        <th>Respuesta Correcta</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($preguntaretos as $preguntareto)
        <tr>
            <td>{!! $preguntareto->reto_id !!}</td>
            <td>{!! $preguntareto->pregunta !!}</td>
            <td>{!! $preguntareto->respuesta_1 !!}</td>
            <td>{!! $preguntareto->respuesta_2 !!}</td>
            <td>{!! $preguntareto->respuesta_correcta !!}</td>
            <td>
                {!! Form::open(['route' => ['preguntaretos.destroy', $preguntareto->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('preguntaretos.show', [$preguntareto->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('preguntaretos.edit', [$preguntareto->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>