<!-- Reto Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reto_id', 'Reto Id:') !!}
    {!! Form::number('reto_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Pregunta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pregunta', 'Pregunta:') !!}
    {!! Form::text('pregunta', null, ['class' => 'form-control']) !!}
</div>

<!-- Respuesta 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('respuesta_1', 'Respuesta 1:') !!}
    {!! Form::text('respuesta_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Respuesta 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('respuesta_2', 'Respuesta 2:') !!}
    {!! Form::text('respuesta_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Respuesta Correcta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('respuesta_correcta', 'Respuesta Correcta:') !!}
    {!! Form::number('respuesta_correcta', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('preguntaretos.index') !!}" class="btn btn-default">Cancel</a>
</div>
