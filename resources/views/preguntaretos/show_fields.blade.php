<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $preguntareto->id !!}</p>
</div>

<!-- Reto Id Field -->
<div class="form-group">
    {!! Form::label('reto_id', 'Reto Id:') !!}
    <p>{!! $preguntareto->reto_id !!}</p>
</div>

<!-- Pregunta Field -->
<div class="form-group">
    {!! Form::label('pregunta', 'Pregunta:') !!}
    <p>{!! $preguntareto->pregunta !!}</p>
</div>

<!-- Respuesta 1 Field -->
<div class="form-group">
    {!! Form::label('respuesta_1', 'Respuesta 1:') !!}
    <p>{!! $preguntareto->respuesta_1 !!}</p>
</div>

<!-- Respuesta 2 Field -->
<div class="form-group">
    {!! Form::label('respuesta_2', 'Respuesta 2:') !!}
    <p>{!! $preguntareto->respuesta_2 !!}</p>
</div>

<!-- Respuesta Correcta Field -->
<div class="form-group">
    {!! Form::label('respuesta_correcta', 'Respuesta Correcta:') !!}
    <p>{!! $preguntareto->respuesta_correcta !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $preguntareto->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $preguntareto->updated_at !!}</p>
</div>

