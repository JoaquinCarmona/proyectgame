@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Preguntareto
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($preguntareto, ['route' => ['preguntaretos.update', $preguntareto->id], 'method' => 'patch']) !!}

                        @include('preguntaretos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection