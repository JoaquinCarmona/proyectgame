<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatemetodologiaRequest;
use App\Http\Requests\UpdatemetodologiaRequest;
use App\Repositories\metodologiaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class metodologiaController extends AppBaseController
{
    /** @var  metodologiaRepository */
    private $metodologiaRepository;

    public function __construct(metodologiaRepository $metodologiaRepo)
    {
        $this->metodologiaRepository = $metodologiaRepo;
    }

    /**
     * Display a listing of the metodologia.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->metodologiaRepository->pushCriteria(new RequestCriteria($request));
        $metodologias = $this->metodologiaRepository->all();

        return view('metodologias.index')
            ->with('metodologias', $metodologias);
    }

    /**
     * Show the form for creating a new metodologia.
     *
     * @return Response
     */
    public function create()
    {
        return view('metodologias.create');
    }

    /**
     * Store a newly created metodologia in storage.
     *
     * @param CreatemetodologiaRequest $request
     *
     * @return Response
     */
    public function store(CreatemetodologiaRequest $request)
    {
        $input = $request->all();

        $metodologia = $this->metodologiaRepository->create($input);

        Flash::success('Metodologia saved successfully.');

        return redirect(route('metodologias.index'));
    }

    /**
     * Display the specified metodologia.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $metodologia = $this->metodologiaRepository->findWithoutFail($id);

        if (empty($metodologia)) {
            Flash::error('Metodologia not found');

            return redirect(route('metodologias.index'));
        }

        return view('metodologias.show')->with('metodologia', $metodologia);
    }

    /**
     * Show the form for editing the specified metodologia.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $metodologia = $this->metodologiaRepository->findWithoutFail($id);

        if (empty($metodologia)) {
            Flash::error('Metodologia not found');

            return redirect(route('metodologias.index'));
        }

        return view('metodologias.edit')->with('metodologia', $metodologia);
    }

    /**
     * Update the specified metodologia in storage.
     *
     * @param  int              $id
     * @param UpdatemetodologiaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatemetodologiaRequest $request)
    {
        $metodologia = $this->metodologiaRepository->findWithoutFail($id);

        if (empty($metodologia)) {
            Flash::error('Metodologia not found');

            return redirect(route('metodologias.index'));
        }

        $metodologia = $this->metodologiaRepository->update($request->all(), $id);

        Flash::success('Metodologia updated successfully.');

        return redirect(route('metodologias.index'));
    }

    /**
     * Remove the specified metodologia from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $metodologia = $this->metodologiaRepository->findWithoutFail($id);

        if (empty($metodologia)) {
            Flash::error('Metodologia not found');

            return redirect(route('metodologias.index'));
        }

        $this->metodologiaRepository->delete($id);

        Flash::success('Metodologia deleted successfully.');

        return redirect(route('metodologias.index'));
    }
}
