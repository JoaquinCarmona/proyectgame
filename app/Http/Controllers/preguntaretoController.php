<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatepreguntaretoRequest;
use App\Http\Requests\UpdatepreguntaretoRequest;
use App\Repositories\preguntaretoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class preguntaretoController extends AppBaseController
{
    /** @var  preguntaretoRepository */
    private $preguntaretoRepository;

    public function __construct(preguntaretoRepository $preguntaretoRepo)
    {
        $this->preguntaretoRepository = $preguntaretoRepo;
    }

    /**
     * Display a listing of the preguntareto.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->preguntaretoRepository->pushCriteria(new RequestCriteria($request));
        $preguntaretos = $this->preguntaretoRepository->all();

        return view('preguntaretos.index')
            ->with('preguntaretos', $preguntaretos);
    }

    /**
     * Show the form for creating a new preguntareto.
     *
     * @return Response
     */
    public function create()
    {
        return view('preguntaretos.create');
    }

    /**
     * Store a newly created preguntareto in storage.
     *
     * @param CreatepreguntaretoRequest $request
     *
     * @return Response
     */
    public function store(CreatepreguntaretoRequest $request)
    {
        $input = $request->all();

        $preguntareto = $this->preguntaretoRepository->create($input);

        Flash::success('Preguntareto saved successfully.');

        return redirect(route('preguntaretos.index'));
    }

    /**
     * Display the specified preguntareto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $preguntareto = $this->preguntaretoRepository->findWithoutFail($id);

        if (empty($preguntareto)) {
            Flash::error('Preguntareto not found');

            return redirect(route('preguntaretos.index'));
        }

        return view('preguntaretos.show')->with('preguntareto', $preguntareto);
    }

    /**
     * Show the form for editing the specified preguntareto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $preguntareto = $this->preguntaretoRepository->findWithoutFail($id);

        if (empty($preguntareto)) {
            Flash::error('Preguntareto not found');

            return redirect(route('preguntaretos.index'));
        }

        return view('preguntaretos.edit')->with('preguntareto', $preguntareto);
    }

    /**
     * Update the specified preguntareto in storage.
     *
     * @param  int              $id
     * @param UpdatepreguntaretoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepreguntaretoRequest $request)
    {
        $preguntareto = $this->preguntaretoRepository->findWithoutFail($id);

        if (empty($preguntareto)) {
            Flash::error('Preguntareto not found');

            return redirect(route('preguntaretos.index'));
        }

        $preguntareto = $this->preguntaretoRepository->update($request->all(), $id);

        Flash::success('Preguntareto updated successfully.');

        return redirect(route('preguntaretos.index'));
    }

    /**
     * Remove the specified preguntareto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $preguntareto = $this->preguntaretoRepository->findWithoutFail($id);

        if (empty($preguntareto)) {
            Flash::error('Preguntareto not found');

            return redirect(route('preguntaretos.index'));
        }

        $this->preguntaretoRepository->delete($id);

        Flash::success('Preguntareto deleted successfully.');

        return redirect(route('preguntaretos.index'));
    }
}
