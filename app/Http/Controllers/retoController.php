<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateretoRequest;
use App\Http\Requests\UpdateretoRequest;
use App\Repositories\retoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class retoController extends AppBaseController
{
    /** @var  retoRepository */
    private $retoRepository;

    public function __construct(retoRepository $retoRepo)
    {
        $this->retoRepository = $retoRepo;
    }

    /**
     * Display a listing of the reto.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->retoRepository->pushCriteria(new RequestCriteria($request));
        $retos = $this->retoRepository->all();

        return view('retos.index')
            ->with('retos', $retos);
    }

    /**
     * Show the form for creating a new reto.
     *
     * @return Response
     */
    public function create()
    {
        return view('retos.create');
    }

    /**
     * Store a newly created reto in storage.
     *
     * @param CreateretoRequest $request
     *
     * @return Response
     */
    public function store(CreateretoRequest $request)
    {
        $input = $request->all();

        $reto = $this->retoRepository->create($input);

        Flash::success('Reto saved successfully.');

        return redirect(route('retos.index'));
    }

    /**
     * Display the specified reto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reto = $this->retoRepository->findWithoutFail($id);

        if (empty($reto)) {
            Flash::error('Reto not found');

            return redirect(route('retos.index'));
        }

        return view('retos.show')->with('reto', $reto);
    }

    /**
     * Show the form for editing the specified reto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reto = $this->retoRepository->findWithoutFail($id);

        if (empty($reto)) {
            Flash::error('Reto not found');

            return redirect(route('retos.index'));
        }

        return view('retos.edit')->with('reto', $reto);
    }

    /**
     * Update the specified reto in storage.
     *
     * @param  int              $id
     * @param UpdateretoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateretoRequest $request)
    {
        $reto = $this->retoRepository->findWithoutFail($id);

        if (empty($reto)) {
            Flash::error('Reto not found');

            return redirect(route('retos.index'));
        }

        $reto = $this->retoRepository->update($request->all(), $id);

        Flash::success('Reto updated successfully.');

        return redirect(route('retos.index'));
    }

    /**
     * Remove the specified reto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $reto = $this->retoRepository->findWithoutFail($id);

        if (empty($reto)) {
            Flash::error('Reto not found');

            return redirect(route('retos.index'));
        }

        $this->retoRepository->delete($id);

        Flash::success('Reto deleted successfully.');

        return redirect(route('retos.index'));
    }
}
