<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\metodologiaRepository;
use App\Repositories\temaRepository;
use App\Repositories\retoRepository;
use App\Repositories\preguntaretoRepository;
use Prettus\Repository\Criteria\RequestCriteria;

class JugadorController extends AppBaseController
{

    private $metodologiaRepository;
    private $temasRepository;
    private $retoRepositoty;
    private $preguntaretoRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(metodologiaRepository $metodologiaRepo, temaRepository $temaRepo, retoRepository $retoRepo, preguntaretoRepository $preguntaretoRepo)
    {
        $this->metodologiaRepository = $metodologiaRepo;
        $this->temasRepository = $temaRepo;
        $this->retoRepositoty = $retoRepo;
        $this->preguntaretoRepository = $preguntaretoRepo;

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('layouts.jugador.home');
    }

    public function inicio_juego (Request $request) {

        $this->metodologiaRepository->pushCriteria(new RequestCriteria( $request ));
        $metodologias = $this->metodologiaRepository->all();

        return view( 'layouts.juego.inicio')->with('metodologias', $metodologias);

    }

    public function inicio_metodologia (Request $request, $id ) {

        $metodologia = $this->metodologiaRepository->findWithoutFail($id);

        $this->temasRepository->pushCriteria(new RequestCriteria( $request ));
        $temas = $this->temasRepository->all();
        $this->retoRepositoty->pushCriteria(new RequestCriteria( $request ));
        $retos = $this->retoRepositoty->all();
        $this->preguntaretoRepository->pushCriteria(new RequestCriteria( $request ));
        $preguntas = $this->preguntaretoRepository->all();

        return view( 'layouts.juego.metodologia', ['metodologia' => $metodologia, 'temas' => $temas , 'retos' => $retos, 'preguntas' => $preguntas ]);

    }

}
