<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class metodologia
 * @package App\Models
 * @version February 11, 2019, 3:08 am UTC
 *
 * @property string nombre
 * @property string descripcion
 * @property string imagen
 * @property integer nivel
 */
class metodologia extends Model
{
    use SoftDeletes;

    public $table = 'metodologias';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'descripcion',
        'imagen',
        'nivel'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'descripcion' => 'string',
        'imagen' => 'string',
        'nivel' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'nivel' => 'required'
    ];

    
}
