<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class preguntareto
 * @package App\Models
 * @version February 11, 2019, 3:22 am UTC
 *
 * @property integer reto_id
 * @property string pregunta
 * @property string respuesta_1
 * @property string respuesta_2
 * @property integer respuesta_correcta
 */
class preguntareto extends Model
{
    use SoftDeletes;

    public $table = 'preguntaretos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'reto_id',
        'pregunta',
        'respuesta_1',
        'respuesta_2',
        'respuesta_correcta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'reto_id' => 'integer',
        'pregunta' => 'string',
        'respuesta_1' => 'string',
        'respuesta_2' => 'string',
        'respuesta_correcta' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'reto_id' => 'required',
        'pregunta' => 'required',
        'respuesta_1' => 'required',
        'respuesta_2' => 'required',
        'respuesta_correcta' => 'required'
    ];

    
}
