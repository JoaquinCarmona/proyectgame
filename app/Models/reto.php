<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class reto
 * @package App\Models
 * @version February 11, 2019, 3:18 am UTC
 *
 * @property string nombre
 * @property integer tema_id
 * @property string descripcion
 * @property string requisitos
 * @property integer requisito_number
 */
class reto extends Model
{
    use SoftDeletes;

    public $table = 'retos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'tema_id',
        'descripcion',
        'requisitos',
        'requisito_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'tema_id' => 'integer',
        'descripcion' => 'string',
        'requisitos' => 'string',
        'requisito_number' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'tema_id' => 'required',
        'descripcion' => 'required',
        'requisitos' => 'required',
        'requisito_number' => 'required'
    ];

    
}
