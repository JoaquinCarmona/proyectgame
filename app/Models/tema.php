<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class tema
 * @package App\Models
 * @version February 11, 2019, 3:10 am UTC
 *
 * @property string titulo
 * @property string descripcion
 * @property integer metodologia_id
 */
class tema extends Model
{
    use SoftDeletes;

    public $table = 'temas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'descripcion',
        'metodologia_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'titulo' => 'string',
        'descripcion' => 'string',
        'metodologia_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'descripcion' => 'required',
        'metodologia_id' => 'required'
    ];

    
}
