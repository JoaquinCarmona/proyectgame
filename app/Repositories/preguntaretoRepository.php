<?php

namespace App\Repositories;

use App\Models\preguntareto;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class preguntaretoRepository
 * @package App\Repositories
 * @version February 11, 2019, 3:22 am UTC
 *
 * @method preguntareto findWithoutFail($id, $columns = ['*'])
 * @method preguntareto find($id, $columns = ['*'])
 * @method preguntareto first($columns = ['*'])
*/
class preguntaretoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'reto_id',
        'pregunta',
        'respuesta_1',
        'respuesta_2',
        'respuesta_correcta'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return preguntareto::class;
    }
}
