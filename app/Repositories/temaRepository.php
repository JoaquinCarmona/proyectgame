<?php

namespace App\Repositories;

use App\Models\tema;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class temaRepository
 * @package App\Repositories
 * @version February 11, 2019, 3:10 am UTC
 *
 * @method tema findWithoutFail($id, $columns = ['*'])
 * @method tema find($id, $columns = ['*'])
 * @method tema first($columns = ['*'])
*/
class temaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'descripcion',
        'metodologia_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tema::class;
    }
}
