<?php

namespace App\Repositories;

use App\Models\reto;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class retoRepository
 * @package App\Repositories
 * @version February 11, 2019, 3:18 am UTC
 *
 * @method reto findWithoutFail($id, $columns = ['*'])
 * @method reto find($id, $columns = ['*'])
 * @method reto first($columns = ['*'])
*/
class retoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'tema_id',
        'descripcion',
        'requisitos',
        'requisito_number'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return reto::class;
    }
}
