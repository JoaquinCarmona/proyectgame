<?php

namespace App\Repositories;

use App\Models\metodologia;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class metodologiaRepository
 * @package App\Repositories
 * @version February 11, 2019, 3:08 am UTC
 *
 * @method metodologia findWithoutFail($id, $columns = ['*'])
 * @method metodologia find($id, $columns = ['*'])
 * @method metodologia first($columns = ['*'])
*/
class metodologiaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'imagen',
        'nivel'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return metodologia::class;
    }
}
